package com.example.sm.service;

import com.example.sm.dto.ChangePassword;
import com.example.sm.dto.Student.CreateStudent;
import com.example.sm.dto.Student.StudentDetail;
import com.example.sm.dto.Student.StudentList;
import com.example.sm.dto.Student.UpdateStudent;

import java.util.List;

public interface StudentService {
    void createStudent(CreateStudent createStudent);
    List<StudentList> getAllStudents();
    StudentDetail getStudent(int id);
    void deleteStudent(int id);
    void updateStudent(int id, UpdateStudent updateStudent);
    float getAverage(int id);
    float getTermAverage(int id, int term);
    void changePassword(ChangePassword password);
}
