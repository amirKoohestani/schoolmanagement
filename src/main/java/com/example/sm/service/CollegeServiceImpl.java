package com.example.sm.service;

import com.example.sm.dao.CollegeDao;
import com.example.sm.dto.college.*;
import com.example.sm.entity.ClassRoom;
import com.example.sm.entity.College;
import com.example.sm.entity.Professor;
import com.example.sm.entity.Student;
import com.example.sm.exception.ExistException;
import com.example.sm.exception.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class CollegeServiceImpl implements CollegeService {
    private final CollegeDao collegeDao;



    public CollegeServiceImpl(CollegeDao collegeDao) {
        this.collegeDao = collegeDao;
    }

    public void createCollege(CreateCollege createCollege) {
        if(collegeDao.existsByName(createCollege.getName()))
            throw new ExistException(
                    "College with name \"" + createCollege.getName() + "\" is already exist");

        College college = new College();

        college.setName(createCollege.getName());

        collegeDao.saveAndFlush(college);
    }

    public List<CollegeList> getAllColleges() {
        List<College> colleges = collegeDao.findAll();
        List<CollegeList> collegeLists = new ArrayList<>();

        for(College college : colleges) {
            CollegeList collegeList = new CollegeList();

            collegeList.setCollegeId(college.getCollegeId());
            collegeList.setName(college.getName());

            collegeLists.add(collegeList);
        }

        return collegeLists;
    }

    public CollegeDetail getCollege(int id) {
        College college = collegeDao.findById(id).orElseThrow(
                () -> new NotFoundException("college with id" + id + "not found")
        );

        Set<ProfessorOfCollege> professorsOfCollege = new HashSet<>();

        for(Professor professor : college.getProfessors()) {
            ProfessorOfCollege professorOfCollege = new ProfessorOfCollege();

            professorOfCollege.setProfessorId(professor.getProfessorId());
            professorOfCollege.setFirsName(professor.getFirstName());
            professorOfCollege.setLastName(professor.getLastName());

            professorsOfCollege.add(professorOfCollege);
        }

        Set<StudentOfCollege> studentsOfCollege = new HashSet<>();

        for(Student student : college.getStudents()) {
            StudentOfCollege studentOfCollege = new StudentOfCollege();

            studentOfCollege.setStudentId(student.getStudentId());
            studentOfCollege.setFirstName(student.getFirstName());
            studentOfCollege.setLastName(student.getLastName());

            studentsOfCollege.add(studentOfCollege);
        }

        CollegeDetail collegeHome = new CollegeDetail();

        collegeHome.setCollegeId(college.getCollegeId());
        collegeHome.setName(college.getName());
        collegeHome.setAverage(this.collegeAverage(college.getCollegeId()));
        collegeHome.setProfessors(professorsOfCollege);
        collegeHome.setStudents(studentsOfCollege);

        return collegeHome;
    }

    public void updateCollege(int id, CollegeDetail collegeHome) {
        College college = collegeDao.findById(id).orElseThrow(
                () -> new NotFoundException(
                        "college with id" + collegeHome.getCollegeId() + "not found")
        );

        if(collegeDao.existsByName(collegeHome.getName()))
            throw new ExistException(
                    "College with name \"" + collegeHome.getName() + "\" is already exist");

        college.setName(collegeHome.getName());

        collegeDao.saveAndFlush(college);
    }

    public void deleteCollege(int id) {
        College college = collegeDao.findById(id).orElseThrow(
                () -> new NotFoundException("college with id" + id + "not found")
        );

            collegeDao.delete(college);
    }

    public float collegeAverage(int id) {
        College college = collegeDao.findById(id).orElseThrow(
                () -> new NotFoundException("college with id" + id + "not found")
        );

        Set<ClassRoom> classRooms = college.getClassRooms();
        int sumUnit = 0;
        float sumScore = 0;

        for(ClassRoom classRoom : classRooms) {
            int unit = classRoom.getStudentNumbers()*classRoom.getLesson().getUnit();
            sumUnit += unit;
            sumScore += classRoom.getAverage()*unit;
        }

        return sumScore/sumUnit;
    }
}
