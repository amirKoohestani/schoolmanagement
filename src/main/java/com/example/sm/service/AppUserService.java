package com.example.sm.service;

import com.example.sm.dao.EmployeeDao;
import com.example.sm.dao.ProfessorDao;
import com.example.sm.dao.StudentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AppUserService implements UserDetailsService
{
    private final StudentDao studentDao;
    private final ProfessorDao professorDao;
    private final EmployeeDao employeeDao;



    public AppUserService(StudentDao studentDao,
                          ProfessorDao professorDao,
                          EmployeeDao employeeDao)
    {
        this.studentDao = studentDao;
        this.professorDao = professorDao;
        this.employeeDao = employeeDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException
    {
        if(studentDao.findByNationalCode(username).isPresent())
            return studentDao.findByNationalCode(username).orElse(null);
        if(professorDao.findByNationalCode(username).isPresent())
            return professorDao.findByNationalCode(username).orElse(null);
        if(employeeDao.findByNationalCode(username).isPresent())
            return employeeDao.findByNationalCode(username).orElse(null);

        throw new UsernameNotFoundException("username not found");
    }
}
