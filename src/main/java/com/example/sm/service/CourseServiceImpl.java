package com.example.sm.service;

import com.example.sm.dao.ClassRoomDao;
import com.example.sm.dao.CourseDao;
import com.example.sm.dao.LessonDao;
import com.example.sm.dao.StudentDao;
import com.example.sm.dto.course.CourseDetail;
import com.example.sm.dto.course.CourseIdDto;
import com.example.sm.dto.course.CreateCourse;
import com.example.sm.entity.ClassRoom;
import com.example.sm.entity.Course;
import com.example.sm.entity.CourseId;
import com.example.sm.exception.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CourseServiceImpl implements CourseService {
    private final CourseDao courseDao;
    private final StudentDao studentDao;
    private final ClassRoomDao classRoomDao;
    private final LessonDao lessonDao;



    public CourseServiceImpl(CourseDao courseDao,
                             StudentDao studentDao,
                             ClassRoomDao classRoomDao,
                             LessonDao lessonDao) {
        this.courseDao = courseDao;
        this.studentDao = studentDao;
        this.classRoomDao = classRoomDao;
        this.lessonDao = lessonDao;
    }

    public void createCourse(CreateCourse createCourse) {
        ClassRoom classRoom = classRoomDao.findById(createCourse.getClassRoom()).orElseThrow(
                () -> new NotFoundException(
                        "ClassRoom with id " + createCourse.getClassRoom() + " not found")
        );

        CourseId courseId = new CourseId();
        courseId.setStudent(studentDao.findById(createCourse.getStudent()).orElseThrow(
                () -> new NotFoundException(
                        "Student with id " + createCourse.getStudent() +" not found")
        ));
        courseId.setLesson(classRoom.getLesson());
        courseId.setTerm(classRoom.getTerm());

        Course course = new Course();

        course.setId(courseId);
        course.setClassRoom(classRoom);
        course.setScore(0.0f);

        courseDao.saveAndFlush(course);
    }

    public CourseDetail getCourse(CourseIdDto courseId) {
        Course course = courseDao.findByIdStudentAndIdLessonAndIdTerm(
                studentDao.findById(courseId.getStudent()).orElseThrow(
                        () -> new NotFoundException(
                                "Student with id " + courseId.getStudent() + " not found")
                ),
                lessonDao.findById(courseId.getLesson()).orElseThrow(
                        () -> new NotFoundException(
                                "Lesson with id " + courseId.getLesson() + " not found")
                ),
                courseId.getTerm()
        ).orElseThrow( () -> new NotFoundException("course not found"));

        CourseDetail courseDetail = new CourseDetail();

        courseDetail.setStudent(course.getId().getStudent().getStudentId());
        courseDetail.setClassRoom(course.getClassRoom().getClassId());
        courseDetail.setScore(course.getScore());

        return courseDetail;
    }

    public void deleteCourse(CourseIdDto courseId) {
        Course course = courseDao.findByIdStudentAndIdLessonAndIdTerm(
                studentDao.findById(courseId.getStudent()).orElseThrow(
                        () -> new NotFoundException(
                                "Student with id " + courseId.getStudent() + " not found")
                ),
                lessonDao.findById(courseId.getLesson()).orElseThrow(
                        () -> new NotFoundException(
                                "Lesson with id " + courseId.getLesson() + " not found")
                ),
                courseId.getTerm()
        ).orElseThrow( () -> new NotFoundException("course not found"));

        courseDao.delete(course);
    }

    public void updateCourse(CourseIdDto courseId, float score) {
        Course course = courseDao.findByIdStudentAndIdLessonAndIdTerm(
                studentDao.findById(courseId.getStudent()).orElseThrow(
                        () -> new NotFoundException(
                                "Student with id " + courseId.getStudent() + " not found")
                ),
                lessonDao.findById(courseId.getLesson()).orElseThrow(
                        () -> new NotFoundException(
                                "Lesson with id " + courseId.getLesson() + " not found")
                ),
                courseId.getTerm()
        ).orElseThrow( () -> new NotFoundException("course not found"));

        course.setScore(score);

        courseDao.saveAndFlush(course);
    }
}
