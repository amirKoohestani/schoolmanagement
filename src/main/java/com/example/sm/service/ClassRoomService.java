package com.example.sm.service;

import com.example.sm.dto.classRoom.ClassRoomDetail;
import com.example.sm.dto.classRoom.ClassRoomList;
import com.example.sm.dto.classRoom.CreateClassRoom;

import java.util.List;

public interface ClassRoomService {
    void createClassRoom(CreateClassRoom saveClassRoom);
    List<ClassRoomList>  getAllClassRooms();
    ClassRoomDetail getClassRoom(int id);
    void deleteClassRoom(int id);
    void updateClassRoom(int id, ClassRoomDetail classRoomHome);
    void addStudent(int classId, int studentId);
    void deleteStudent(int classId, int studentId);
}
