package com.example.sm.service;

import com.example.sm.dao.CollegeDao;
import com.example.sm.dao.CourseDao;
import com.example.sm.dao.StudentDao;
import com.example.sm.dto.ChangePassword;
import com.example.sm.dto.Student.*;
import com.example.sm.entity.ClassRoom;
import com.example.sm.entity.Course;
import com.example.sm.entity.Student;
import com.example.sm.exception.ExistException;
import com.example.sm.exception.NotFoundException;
import com.example.sm.exception.WrongPasswordException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {
    private final StudentDao studentDao;
    private final CollegeDao collegeDao;
    private final CourseDao courseDao;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;



    public StudentServiceImpl(StudentDao studentDao,
                              CollegeDao collegeDao,
                              CourseDao courseDao,
                              BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.studentDao = studentDao;
        this.collegeDao = collegeDao;
        this.courseDao = courseDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void createStudent(CreateStudent createStudent) {
        if(studentDao.existsByNationalCode(createStudent.getNationalCode()))
            throw new ExistException("student already exist");

        Student student = new Student();

        student.setFirstName(createStudent.getFirstName());
        student.setLastName(createStudent.getLastName());
        student.setNationalCode(createStudent.getNationalCode());
        student.setAddress(createStudent.getAddress());
        student.setCollege(collegeDao.findById(createStudent.getCollege()).orElseThrow(
                () -> new NotFoundException("College with id " + createStudent.getCollege() + " not found")
        ));

        studentDao.saveAndFlush(student);
    }

    public List<StudentList> getAllStudents() {
        List<Student> students = studentDao.findAll();
        List<StudentList> studentLists = new ArrayList<>();

        for(Student student : students) {
            StudentList studentList = new StudentList();

            studentList.setStudentId(student.getStudentId());
            studentList.setFirstName(student.getFirstName());
            studentList.setLastname(student.getLastName());
            studentList.setNationalCode(student.getNationalCode());
            studentList.setCollege(student.getCollege().getName());
            studentList.setAverage(student.getAverage());

            studentLists.add(studentList);
        }

        return studentLists;
    }

    public StudentDetail getStudent(int id) {
        Student student = studentDao.findById(id).orElseThrow(
                () -> new NotFoundException("student with id " + id + " not found!")
        );

        student.setAverage(this.getAverage(student.getStudentId()));

        Set<StudentsClass> studentsClasses = new HashSet<>();

        for(ClassRoom classRoom : student.getClassRooms()) {
            StudentsClass studentsClass = new StudentsClass();

            studentsClass.setClassId(classRoom.getClassId());
            studentsClass.setProfessor(classRoom.getProfessor().getLastName());
            studentsClass.setLesson(classRoom.getLesson().getName());
            studentsClass.setTerm(classRoom.getTerm());
            studentsClass.setCollege(classRoom.getCollege().getName());
            studentsClass.setScore(courseDao.findByIdStudentAndClassRoom(student, classRoom).orElseThrow(
                    () -> new NotFoundException("course was not created")).getScore()
            );

            studentsClasses.add(studentsClass);
        }

        StudentDetail studentDetail = new StudentDetail();

        studentDetail.setStudentId(student.getStudentId());
        studentDetail.setFirstName(student.getFirstName());
        studentDetail.setLastName(student.getLastName());
        studentDetail.setNationalCode(student.getNationalCode());
        studentDetail.setAddress(student.getAddress());
        studentDetail.setCollege(student.getCollege().getName());
        studentDetail.setStudentsClasses(studentsClasses);
        studentDetail.setAverage(student.getAverage());

        return studentDetail;
    }

    public void deleteStudent(int id) {
        Student student = studentDao.findById(id).orElseThrow(
                () -> new NotFoundException("student with id " + id + " not found!")
        );

        studentDao.delete(student);
    }

    public void updateStudent(int id, UpdateStudent updateStudent) {
        Student student = studentDao.findById(id).orElseThrow(
                () -> new NotFoundException("student with id " + id + " not found!")
        );

        student.setCollege(updateStudent.getCollege());
        student.setAddress(updateStudent.getAddress());

        studentDao.saveAndFlush(student);
    }

    public float getAverage(int id) {
        Student  student = studentDao.findById(id).orElseThrow(
                () -> new NotFoundException("student with id " + id + " not found")
        );

        Set<Course> courses = student.getCourseList();

        return determineAverage(courses);
    }

    public float getTermAverage(int id, int term) {
        Set<Course> courses = courseDao.findByIdStudentAndIdTerm(studentDao.findById(id).orElseThrow(
                () -> new NotFoundException("student with id " + id + " not found!")
        ), term);

        return determineAverage(courses);
    }

    public float determineAverage(Set<Course> courses){
        int sumUnit = 0;
        float sumScore = 0.0f;

        if (courses.size() == 0)
            return 0;

        for(Course course  : courses) {
            int unit = course.getId().getLesson().getUnit();
            sumUnit += unit;
            sumScore += course.getScore()*unit;
        }

        return  sumScore / sumUnit;
    }

    public void changePassword(ChangePassword password) {
        Student student = studentDao.findById(password.getId()).orElseThrow(
                () -> new NotFoundException("student with id " + password.getId() + " not found!")
        );

        if(bCryptPasswordEncoder.matches(password.getOldPassword(), student.getPassword()))
            student.setPassword(bCryptPasswordEncoder.encode(password.getNewPassword()));
        else
            throw new WrongPasswordException("password is incorrect");
    }
}