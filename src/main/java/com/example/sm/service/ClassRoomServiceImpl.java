package com.example.sm.service;

import com.example.sm.dao.*;
import com.example.sm.dto.classRoom.ClassRoomDetail;
import com.example.sm.dto.classRoom.ClassRoomList;
import com.example.sm.dto.classRoom.StudentOfClass;
import com.example.sm.dto.classRoom.CreateClassRoom;
import com.example.sm.dto.course.CourseIdDto;
import com.example.sm.dto.course.CreateCourse;
import com.example.sm.entity.ClassRoom;
import com.example.sm.entity.Course;
import com.example.sm.entity.Student;
import com.example.sm.exception.ExistException;
import com.example.sm.exception.FullClassException;
import com.example.sm.exception.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class ClassRoomServiceImpl implements ClassRoomService {
    private final ClassRoomDao classRoomDao;
    private final ProfessorDao professorDao;
    private final LessonDao lessonDao;
    private final CollegeDao collegeDao;
    private final CourseDao courseDao;
    private final StudentDao studentDao;
    private final CourseService courseService;



    public ClassRoomServiceImpl(ClassRoomDao classRoomDao,
                                ProfessorDao professorDao,
                                LessonDao lessonDao,
                                CollegeDao collegeDao,
                                CourseDao courseDao,
                                StudentDao studentDao,
                                CourseService courseService) {
        this.classRoomDao = classRoomDao;
        this.professorDao = professorDao;
        this.lessonDao = lessonDao;
        this.collegeDao = collegeDao;
        this.courseDao = courseDao;
        this.studentDao = studentDao;
        this.courseService = courseService;
    }


    public void createClassRoom(CreateClassRoom saveClassRoom) {
        ClassRoom classRoom = new ClassRoom();

        classRoom.setProfessor(professorDao.findById(saveClassRoom.getProfessor()).orElseThrow(
                () -> new NotFoundException("Professor not found")
        ));
        classRoom.setLesson(lessonDao.findById(saveClassRoom.getLesson()).orElseThrow(
                () -> new NotFoundException("Lesson not found")
        ));
        classRoom.setTerm(saveClassRoom.getTerm());
        classRoom.setCollege(collegeDao.findById(saveClassRoom.getCollege()).orElseThrow(
                () -> new NotFoundException("College not found")
        ));
        classRoom.setCapacity(saveClassRoom.getCapacity());
        classRoom.setStudentNumbers(0);
        classRoom.setAverage(0.0f);

        classRoomDao.saveAndFlush(classRoom);
    }

    public List<ClassRoomList>  getAllClassRooms() {
        List<ClassRoom> classRooms = classRoomDao.findAll();
        List<ClassRoomList> classRoomLists = new ArrayList<>();

        for(ClassRoom classRoom : classRooms) {
            ClassRoomList classRoomList = new ClassRoomList();

            classRoomList.setClassId(classRoom.getClassId());
            classRoomList.setProfessor(classRoom.getProfessor().getProfessorId());
            classRoomList.setLesson(classRoom.getLesson().getLessonId());
            classRoomList.setTerm(classRoom.getTerm());
            classRoomList.setCollege(classRoom.getCollege().getCollegeId());
            classRoomList.setCapacity(classRoom.getCapacity());
            classRoomList.setStudentNumbers(classRoom.getStudentNumbers());

            classRoomLists.add(classRoomList);
        }

        return classRoomLists;
    }

    public ClassRoomDetail getClassRoom(int id) {
        ClassRoom classRoom = classRoomDao.findById(id).orElseThrow(
                () -> new NotFoundException("ClassRoom with id " + id + " not found!")
        );

        Set<StudentOfClass> studentsOfClass = new HashSet<>();

        for(Student student : classRoom.getStudents()) {
            StudentOfClass studentOfClass = new StudentOfClass();

            studentOfClass.setFirstName(student.getFirstName());
            studentOfClass.setLastName(student.getLastName());
            studentOfClass.setStudentId(student.getStudentId());
            studentOfClass.setCollege(student.getCollege().getName());

            studentsOfClass.add(studentOfClass);
        }

        classRoom.setAverage(this.determineAverage(id));

        ClassRoomDetail classRoomDetail = new ClassRoomDetail();

        classRoomDetail.setClassId(classRoom.getClassId());
        classRoomDetail.setProfessor(classRoom.getProfessor().getProfessorId());
        classRoomDetail.setLesson(classRoom.getLesson().getLessonId());
        classRoomDetail.setTerm(classRoom.getTerm());
        classRoomDetail.setCollege(classRoom.getCollege().getCollegeId());
        classRoomDetail.setCapacity(classRoom.getCapacity());
        classRoomDetail.setStudentNumbers(classRoom.getStudentNumbers());
        classRoomDetail.setStudents(studentsOfClass);
        classRoomDetail.setAverage(classRoom.getAverage());

        return classRoomDetail;
    }

    public void deleteClassRoom(int id) {
        ClassRoom classRoom = classRoomDao.findById(id).orElseThrow(
                () -> new NotFoundException("classRoom with id " + id + " not found!")
        );

        classRoomDao.delete(classRoom);
    }

    public void updateClassRoom(int id, ClassRoomDetail classRoomHome) {
        ClassRoom classRoom = classRoomDao.findById(id).orElseThrow(
                () -> new NotFoundException("classRoom with id " + id + " not found!")
        );

        classRoom.setProfessor(professorDao.findById(classRoomHome.getProfessor()).orElseThrow(
                () -> new NotFoundException("Professor not found")
        ));
        classRoom.setLesson(lessonDao.findById(classRoomHome.getLesson()).orElseThrow(
                () -> new NotFoundException("Lesson not found")
        ));
        classRoom.setTerm(classRoomHome.getTerm());
        classRoom.setCollege(collegeDao.findById(classRoomHome.getCollege()).orElseThrow(
                () -> new NotFoundException("College not found")
        ));
        classRoom.setCapacity(classRoomHome.getCapacity());
        classRoom.setAverage(this.determineAverage(id));

        classRoomDao.saveAndFlush(classRoom);
    }

    public float determineAverage(int id) {
        ClassRoom classRoom = classRoomDao.findById(id).orElseThrow(
                () -> new NotFoundException("classRoom with id " + id + " not found!")
        );
        Set<Course> courses = classRoom.getCourses();

        float sumScore = 0;

        for(Course course : courses)
            sumScore += course.getScore();

        return sumScore / courses.size();
    }

    public void addStudent(int classId, int studentId) {
        ClassRoom classRoom = classRoomDao.findById(classId).orElseThrow(
                () -> new NotFoundException("class with id " + classId + " not found")
        );
        Student student = studentDao.findById(studentId).orElseThrow(
                () -> new NotFoundException("student with id " + studentId + " not found!")
        );

        courseDao.findByIdStudentAndIdLessonAndIdTerm( student,
                classRoom.getLesson(), classRoom.getTerm()).orElseThrow(
                () -> new ExistException("this lesson has already taken by the student")
        );

        if(classRoom.getCapacity() <= classRoom.getStudentNumbers())
            throw new FullClassException("the class is full");

        classRoom.setStudentNumbers(classRoom.getStudentNumbers()+1);
        classRoom.getStudents().add(student);

        CreateCourse createCourse = new CreateCourse();

        createCourse.setStudent(studentId);
        createCourse.setClassRoom(classId);

        courseService.createCourse(createCourse);
        classRoomDao.saveAndFlush(classRoom);
    }

    public void deleteStudent(int classId, int studentId) {
        ClassRoom classRoom = classRoomDao.findById(classId).orElseThrow(
                () -> new NotFoundException("class with id " + classId + " not found")
        );
        Student student = studentDao.findById(studentId).orElseThrow(
                () -> new NotFoundException("student with id " + studentId + " not found!")
        );

        courseDao.findByIdStudentAndIdLessonAndIdTerm( student,
                classRoom.getLesson(), classRoom.getTerm()).orElseThrow(
                () -> new NotFoundException("this class is not taken by student")
        );

        classRoom.setStudentNumbers(classRoom.getStudentNumbers()-1);
        classRoom.getStudents().remove(student);

        CourseIdDto courseId = new CourseIdDto();

        courseId.setStudent(studentId);
        courseId.setLesson(classRoom.getLesson().getLessonId());
        courseId.setTerm(classRoom.getTerm());

        courseService.deleteCourse(courseId);
        classRoomDao.saveAndFlush(classRoom);
    }
}