package com.example.sm.service;

import com.example.sm.dto.lesson.CreateLesson;
import com.example.sm.dto.lesson.LessonDetail;
import com.example.sm.dto.lesson.UpdateLesson;

import java.util.List;

public interface LessonService {
    void createLesson(CreateLesson createLesson);
    List<LessonDetail> getAllLessons();
    LessonDetail getLesson(int id);
    void deleteLesson(int id);
    void updateLesson(int id, UpdateLesson updateLesson);
}
