package com.example.sm.service;

import com.example.sm.dao.EmployeeDao;
import com.example.sm.dto.ChangePassword;
import com.example.sm.dto.employee.CreateEmployee;
import com.example.sm.dto.employee.EmployeeDetail;
import com.example.sm.entity.Employee;
import com.example.sm.entity.UserRole;
import com.example.sm.exception.ExistException;
import com.example.sm.exception.NotFoundException;
import com.example.sm.exception.WrongPasswordException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeDao employeeDao;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;



    public EmployeeServiceImpl(EmployeeDao employeeDao,
                               BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.employeeDao = employeeDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void createEmployee(CreateEmployee createEmployee) {
        if(employeeDao.existsByNationalCode(createEmployee.getNationalCode()))
            throw new ExistException("Employee already exist");

        Employee employee = new Employee();

        employee.setFirstName(createEmployee.getFirstName());
        employee.setLastName(createEmployee.getLastName());
        employee.setNationalCode(createEmployee.getNationalCode());
        employee.setPassword(createEmployee.getNationalCode());
        employee.setUserRole(UserRole.INSTRUCTION_EMPLOYEE);

        employeeDao.saveAndFlush(employee);
    }

    public List<EmployeeDetail> getAllEmployees() {
        List<Employee> employees = employeeDao.findAll();
        List<EmployeeDetail> employeeDetailList = new ArrayList<>();

        for(Employee employee : employees) {
            EmployeeDetail employeeDetail = new EmployeeDetail();

            employeeDetail.setPersonnelId(employee.getPerssonelId());
            employeeDetail.setFirstName(employee.getFirstName());
            employeeDetail.setLastName(employee.getLastName());
            employeeDetail.setNationalCode(employee.getNationalCode());

            employeeDetailList.add(employeeDetail);
        }

        return employeeDetailList;
    }

    public EmployeeDetail getEmployee(int id) {
        Employee employee = employeeDao.findById(id).orElseThrow(() ->
                new NotFoundException("Employee with id " + id + " not found")
        );

        EmployeeDetail employeeDetail = new EmployeeDetail();

        employeeDetail.setPersonnelId(employee.getPerssonelId());
        employeeDetail.setFirstName(employee.getFirstName());
        employeeDetail.setLastName(employee.getLastName());
        employeeDetail.setNationalCode(employee.getNationalCode());

        return employeeDetail;
    }

    public void deleteEmployee(int id) {
        Employee employee = employeeDao.findById(id).orElseThrow(
                () -> new NotFoundException("employee with id " + id + " not found")
        );

        employeeDao.delete(employee);
    }

    public void updateEmployee(int id, EmployeeDetail employeeDetail) {
        Employee employee = employeeDao.findById(id).orElseThrow(
                () -> new NotFoundException("employee with id" + id + "not found")
        );

        employee.setFirstName(employeeDetail.getFirstName());
        employee.setLastName(employeeDetail.getLastName());
        employee.setNationalCode(employeeDetail.getNationalCode());

        employeeDao.saveAndFlush(employee);
    }

    public void changePassword(ChangePassword password) {
        Employee employee = employeeDao.findById(password.getId()).orElseThrow(
                () -> new NotFoundException("employee with id " + password.getId() + " not found!")
        );

        if(bCryptPasswordEncoder.matches(password.getOldPassword(), employee.getPassword()))
            employee.setPassword(bCryptPasswordEncoder.encode(password.getNewPassword()));
        else
            throw new WrongPasswordException("password is incorrect");
    }
}