package com.example.sm.service;

import com.example.sm.dto.ChangePassword;
import com.example.sm.dto.professor.CreateProfessor;
import com.example.sm.dto.professor.ProfessorDetail;
import com.example.sm.dto.professor.ProfessorList;
import com.example.sm.dto.professor.UpdateProfessor;

import java.util.List;

public interface ProfessorService {
    void createProfessor(CreateProfessor createProfessor);
    List<ProfessorList> getAllProfessors();
    ProfessorDetail getProfessor(int id);
    void deleteProfessor(int id);
    void updateProfessor(int id, UpdateProfessor updateProfessor);
    float getStudentsAverage(int id);
    float getTermAverage(int id, int term);
    float getLessonsAverage(int id, int lesson);
    void changePassword(ChangePassword password);
}
