package com.example.sm.service;

import com.example.sm.dao.ClassRoomDao;
import com.example.sm.dao.CollegeDao;
import com.example.sm.dao.LessonDao;
import com.example.sm.dao.ProfessorDao;
import com.example.sm.dto.ChangePassword;
import com.example.sm.dto.professor.*;
import com.example.sm.entity.ClassRoom;
import com.example.sm.entity.Professor;
import com.example.sm.entity.UserRole;
import com.example.sm.exception.ExistException;
import com.example.sm.exception.NotFoundException;
import com.example.sm.exception.WrongPasswordException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class ProfessorServiceImpl implements ProfessorService {
    private final ProfessorDao professorDao;
    private final CollegeDao collegeDao;
    private final ClassRoomDao classRoomDao;
    private final LessonDao lessonDao;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;



    public ProfessorServiceImpl(ProfessorDao professorDao,
                                CollegeDao collegeDao,
                                ClassRoomDao classRoomDao,
                                LessonDao lessonDao,
                                BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.professorDao = professorDao;
        this.collegeDao = collegeDao;
        this.classRoomDao = classRoomDao;
        this.lessonDao = lessonDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void createProfessor(CreateProfessor createProfessor) {
        if(professorDao.existsByNationalCode(createProfessor.getNationalCode()))
            throw new ExistException("professor already exist");

        Professor professor = new Professor();

        professor.setFirstName(createProfessor.getFirstName());
        professor.setLastName(createProfessor.getLastName());
        professor.setNationalCode(createProfessor.getNationalCode());
        professor.setPassword(bCryptPasswordEncoder.encode(createProfessor.getNationalCode()));
        professor.setAddress(createProfessor.getAddress());
        professor.setCollege(collegeDao.findById(createProfessor.getCollege()).orElseThrow(
                () -> new NotFoundException("college with id " + createProfessor.getCollege() + " not found")
        ));
        professor.setUserRole(UserRole.PROFESSOR);

        professorDao.saveAndFlush(professor);
    }

    public List<ProfessorList> getAllProfessors() {
        List<Professor> professors = professorDao.findAll();
        List<ProfessorList> professorLists = new ArrayList<>();

        for(Professor professor : professors)
        {
            ProfessorList professorList = new ProfessorList();

            professorList.setProfessorId(professor.getProfessorId());
            professorList.setFirstName(professor.getFirstName());
            professorList.setLastName(professor.getLastName());
            professorList.setNationalCode(professor.getNationalCode());
            professorList.setCollege(professor.getCollege().getName());

            professorLists.add(professorList);
        }

        return professorLists;
    }

    public ProfessorDetail getProfessor(int id) {
        Professor professor = professorDao.findById(id).orElseThrow(
                () -> new NotFoundException("professor with id " + id + " not found")
        );

        Set<ProfessorsClass> professorsClasses = new HashSet<>();

        for(ClassRoom classRoom : professor.getClassRooms()) {
            ProfessorsClass professorsClass = new ProfessorsClass();

            professorsClass.setClassId(classRoom.getClassId());
            professorsClass.setLesson(classRoom.getLesson().getName());
            professorsClass.setTerm(classRoom.getTerm());
            professorsClass.setCollege(classRoom.getCollege().getName());
            professorsClass.setAverage(classRoom.getAverage());

            professorsClasses.add(professorsClass);
        }

        ProfessorDetail professorDetail = new ProfessorDetail();

        professorDetail.setProfessorId(professor.getProfessorId());
        professorDetail.setFirstName(professor.getFirstName());
        professorDetail.setLastName(professor.getLastName());
        professorDetail.setNationalCode(professor.getNationalCode());
        professorDetail.setAddress(professor.getAddress());
        professorDetail.setCollege(professor.getCollege().getName());
        professorDetail.setProfessorClasses(professorsClasses);
        professorDetail.setStudentsAverage(this.getStudentsAverage(id));

        return professorDetail;
    }

    public void deleteProfessor(int id) {
        Professor professor = professorDao.findById(id).orElseThrow(
                () -> new NotFoundException("professor with id " + id + "not found")
        );

        professorDao.delete(professor);
    }

    public void updateProfessor(int id, UpdateProfessor updateProfessor) {
        Professor professor = professorDao.findById(id).orElseThrow(
                () -> new NotFoundException(
                        "professor with id " + id + " not found!")
        );

        professor.setAddress(updateProfessor.getAddress());
        professor.setCollege(updateProfessor.getCollege());

        professorDao.saveAndFlush(professor);
    }

    public float getStudentsAverage(int id) {
        Professor professor = professorDao.findById(id).orElseThrow(
                () -> new NotFoundException("professor with id " + id + " not found")
        );

        Set<ClassRoom> classRooms = professor.getClassRooms();

        return determineAverage(classRooms);
    }

    public float getTermAverage(int id, int term) {
        Set<ClassRoom> classRooms = classRoomDao.findByProfessorAndTerm(
                professorDao.findById(id).orElseThrow(
                        () -> new NotFoundException("professor with id " + id + " not found")
                ), term);

        return determineAverage(classRooms);
    }

    public float getLessonsAverage(int id, int lesson) {
        Set<ClassRoom> classRooms = classRoomDao.findByProfessorAndLesson(
                professorDao.findById(id).orElseThrow(
                        () -> new NotFoundException("professor with id " + id + " not found")
                ),
                lessonDao.findById(lesson).orElseThrow(
                        () -> new NotFoundException("lesson with id " + lesson + " not found")
                ));

        return determineAverage(classRooms);
    }

    public float determineAverage(Set<ClassRoom> classRooms){
        int sumUnit = 0;
        float sumScore = 0;

        for(ClassRoom classRoom : classRooms) {
            int unit = classRoom.getStudentNumbers()*classRoom.getLesson().getUnit();
            sumUnit += unit;
            sumScore += classRoom.getAverage()*unit;
        }
        return sumScore/sumUnit;
    }

    public void changePassword(ChangePassword password) {
        Professor professor = professorDao.findById(password.getId()).orElseThrow(
                () -> new NotFoundException("professor with id " + password.getId() + " not found!")
        );

        if(bCryptPasswordEncoder.matches(password.getOldPassword(), professor.getPassword()))
            professor.setPassword(bCryptPasswordEncoder.encode(password.getNewPassword()));
        else
            throw new WrongPasswordException("password is incorrect");
    }
}
