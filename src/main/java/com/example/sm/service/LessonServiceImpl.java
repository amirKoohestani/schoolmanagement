package com.example.sm.service;

import com.example.sm.dao.LessonDao;
import com.example.sm.dto.lesson.CreateLesson;
import com.example.sm.dto.lesson.LessonDetail;
import com.example.sm.dto.lesson.UpdateLesson;
import com.example.sm.entity.Lesson;
import com.example.sm.exception.ExistException;
import com.example.sm.exception.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class LessonServiceImpl implements LessonService {
    private final LessonDao lessonDao;



    public LessonServiceImpl(LessonDao lessonDao) {
        this.lessonDao = lessonDao;
    }

    public void createLesson(CreateLesson createLesson) {
        if(lessonDao.existsByNameAndUnit(createLesson.getName(), createLesson.getUnit()))
            throw new ExistException("Lesson already exist");

        Lesson lesson = new Lesson();

        lesson.setName(createLesson.getName());
        lesson.setUnit(createLesson.getUnit());

        lessonDao.saveAndFlush(lesson);
    }

    public List<LessonDetail> getAllLessons() {
        List<Lesson> lessons = lessonDao.findAll();
        List<LessonDetail> lessonDetailList = new ArrayList<>();

        for(Lesson lesson : lessons) {
            LessonDetail lessonDetail = new LessonDetail();

            lessonDetail.setLessonId(lesson.getLessonId());
            lessonDetail.setName(lesson.getName());
            lessonDetail.setUnit(lesson.getUnit());

            lessonDetailList.add(lessonDetail);
        }

        return lessonDetailList;
    }

    public LessonDetail getLesson(int id) {
        Lesson lesson = lessonDao.findById(id).orElseThrow(
                () -> new NotFoundException("lesson with id " + id + " not found")
        );

        LessonDetail lessonDetail = new LessonDetail();

        lessonDetail.setLessonId(lesson.getLessonId());
        lessonDetail.setName(lesson.getName());
        lessonDetail.setUnit(lesson.getUnit());

        return lessonDetail;
    }

    public void deleteLesson(int id) {
        Lesson lesson = lessonDao.findById(id).orElseThrow(
                () -> new NotFoundException("lesson with id " + id + " not found")
        );

        lessonDao.delete(lesson);
    }

    public void updateLesson(int id, UpdateLesson updateLesson) {
        Lesson lesson = lessonDao.findById(id).orElseThrow(
                () -> new NotFoundException("lesson with id " + id + " not found")
        );

        lesson.setUnit(updateLesson.getUnit());
        lesson.setName(updateLesson.getName());

        lessonDao.saveAndFlush(lesson);
    }
}
