package com.example.sm.service;

import com.example.sm.dto.college.CollegeDetail;
import com.example.sm.dto.college.CollegeList;
import com.example.sm.dto.college.CreateCollege;

import java.util.List;

public interface CollegeService {
    void createCollege(CreateCollege createCollege);
    List<CollegeList> getAllColleges();
    CollegeDetail getCollege(int id);
    void updateCollege(int id, CollegeDetail collegeHome);
    void deleteCollege(int id);
}
