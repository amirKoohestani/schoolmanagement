package com.example.sm.service;

import com.example.sm.dto.ChangePassword;
import com.example.sm.dto.employee.CreateEmployee;
import com.example.sm.dto.employee.EmployeeDetail;

import java.util.List;

public interface EmployeeService {
    void createEmployee(CreateEmployee createEmployee);
    List<EmployeeDetail> getAllEmployees();
    EmployeeDetail getEmployee(int id);
    void deleteEmployee(int id);
    void updateEmployee(int id, EmployeeDetail employeeDetail);
    void changePassword(ChangePassword password);
}
