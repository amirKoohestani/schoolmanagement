package com.example.sm.service;

import com.example.sm.dto.course.CourseDetail;
import com.example.sm.dto.course.CourseIdDto;
import com.example.sm.dto.course.CreateCourse;

public interface CourseService {
    void createCourse(CreateCourse createCourse);
    CourseDetail getCourse(CourseIdDto courseId);
    void deleteCourse(CourseIdDto courseId);
    void updateCourse(CourseIdDto courseId, float score);
}
