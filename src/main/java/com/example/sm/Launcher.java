package com.example.sm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Launcher
{
    private static ConfigurableApplicationContext context;


    public static void main(String[] args)
    {
        context = SpringApplication.run(Launcher.class, args);

//        createLesson();
//        findLesson();
//        updateLesson();
//        deleteLesson();
//        getAllLesson();

//        saveCollege();
//        findCollege();
//        updateCollege();
//        deleteCollege();
//        getAllCollege();

//        createProfessor();
//        getProfessor();
//        updateProfessor();
//        deleteProfessor();
//        getAllProfessor();

//        createStudent();
//        findStudent();
//        updateStudent();
//        deleteStudent();
//        getAllStudent();

//        createClassRoom();
//        pickCourseByStudent();
//        findClassRoom();
//        updateClassRoom();
//        deleteCourseByStudent();
//        deleteClassRoom();
    }
//
//    public static void saveLesson()
//    {
//        LessonService lessonService = context.getBean(LessonService.class);
//
//        CreateLesson saveLesson = new CreateLesson();
//        saveLesson.setName("java");
//        saveLesson.setUnit(3);
//
//        try {
//            lessonService.saveLesson(saveLesson);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void findLesson()
//    {
//        LessonService lessonService = context.getBean(LessonService.class);
//
//        LessonDetail lessonHome = new LessonDetail();
//
//        try {
//            lessonHome = lessonService.findLesson(1);
//            System.out.println(lessonHome);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void updateLesson()
//    {
//        LessonService lessonService = context.getBean(LessonService.class);
//
//        LessonDetail lessonHome = new LessonDetail();
//        lessonHome.setLessonId(1);
//        lessonHome.setName("programming");
//        lessonHome.setUnit(4);
//
//        try {
//            lessonService.updateLesson(lessonHome);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void deleteLesson()
//    {
//        LessonService lessonService = context.getBean(LessonService.class);
//
//        try {
//            lessonService.deleteLesson(1);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void getAllLesson()
//    {
//        LessonService lessonService = context.getBean(LessonService.class);
//
//        List<LessonDetail> lessonHomes = lessonService.getAll();
//
//        for(LessonDetail lessonHome : lessonHomes)
//            System.out.println(lessonHome);
//    }
//
//    public static void saveCollege()
//    {
//        CollegeService collegeService = context.getBean(CollegeService.class);
//
//        CreateCollege saveCollege = new CreateCollege();
//        saveCollege.setName("Computer");
//
//        try {
//            collegeService.saveCollege(saveCollege);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void findCollege()
//    {
//        CollegeService collegeService = context.getBean(CollegeService.class);
//
//        try {
//            CollegeDetail collegeHome = collegeService.findCollege(1);
//            System.out.println(collegeHome);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void updateCollege()
//    {
//        CollegeService collegeService = context.getBean(CollegeService.class);
//
//        CollegeDetail collegeHome = new CollegeDetail();
//        collegeHome.setCollegeId(1);
//        collegeHome.setName("Computer Engineering");
//
//        try {
//            collegeService.updateCollege(collegeHome);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void deleteCollege()
//    {
//        CollegeService collegeService = context.getBean(CollegeService.class);
//
//        try {
//            collegeService.deleteCollege(1);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void getAllCollege()
//    {
//        CollegeService collegeService = context.getBean(CollegeService.class);
//
//        List<CollegeList> colleges = collegeService.getAll();
//
//        for(CollegeList collegeList : colleges)
//            System.out.println(collegeList);
//    }
//
//    public static void saveProfessor()
//    {
//        ProfessorService professorService = context.getBean(ProfessorService.class);
//
//        CreateProfessor saveProfessor = new CreateProfessor();
//        saveProfessor.setFirstName("reza");
//        saveProfessor.setLastName("mohammadi");
//        saveProfessor.setNationalCode("0123456789");
//        saveProfessor.setCollege(2);
//        saveProfessor.setProvince("Tehran");
//        saveProfessor.setTownship("Tehran");
//        saveProfessor.setCity("Andishe");
//
//        try {
//            professorService.saveProfessor(saveProfessor);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void findProfessor()
//    {
//        ProfessorService professorService = context.getBean(ProfessorService.class);
//
//        try {
//            ProfessorDetail professorHome = professorService.getProfessor(1);
//            System.out.println(professorHome);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void updateProfessor()
//    {
//        ProfessorService professorService = context.getBean(ProfessorService.class);
//
//        ProfessorDetail professorHome = new ProfessorDetail();
//        professorHome.setProfessorId(1);
//        professorHome.setFirstName("ahmad");
//
//        try {
//            professorService.updateProfessor(professorHome);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void deleteProfessor()
//    {
//        ProfessorService professorService = context.getBean(ProfessorService.class);
//
//        try {
//            professorService.deleteProfessor(1);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void getAllProfessor()
//    {
//        ProfessorService professorService = context.getBean(ProfessorService.class);
//
//        List<ProfessorList> professorLists = professorService.getAll();
//
//        for(ProfessorList professorList : professorLists)
//            System.out.println(professorList);
//    }
//
//    public static void saveStudent()
//    {
//        StudentService studentService = context.getBean(StudentService.class);
//
//        CreateStudent saveStudent = new CreateStudent();
//        saveStudent.setFirstName("Amir");
//        saveStudent.setLastName("koohstani");
//        saveStudent.setNationalCode("9876543210");
//        saveStudent.setCollege(2);
//        saveStudent.setProvince("Tehran");
//        saveStudent.setTownship("Tehran");
//        saveStudent.setCity("malard");
//        saveStudent.setOther("pelak 1");
//        saveStudent.setPostcode("5544332211");
//
//        try {
//            studentService.saveStudent(saveStudent);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void findStudent()
//    {
//        StudentService studentService = context.getBean(StudentService.class);
//
//        try {
//            StudentDetail studentHome = studentService.findStudent(1);
//            System.out.println(studentHome);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void updateStudent()
//    {
//        StudentService studentService = context.getBean(StudentService.class);
//
//        StudentDetail studentHome = new StudentDetail();
//        studentHome.setStudentId(1);
//        studentHome.setFirstName("Amir hossein");
//
//        try {
//            studentService.updateStudent(studentHome);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void deleteStudent()
//    {
//        StudentService studentService = context.getBean(StudentService.class);
//
//        try {
//            studentService.deleteStudent(1);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void getAllStudent()
//    {
//        StudentService studentService = context.getBean(StudentService.class);
//
//        List<StudentList> studentLists = studentService.getAll();
//
//        for(StudentList studentList : studentLists)
//            System.out.println(studentList);
//    }
//
//    public static void createClassRoom() throws ExceptionMessage
//    {
//        ClassRoomService classRoomService = context.getBean(ClassRoomService.class);
//
//        CreateClassRoom saveClassRoom = new CreateClassRoom();
//        saveClassRoom.setLesson(2);
//        saveClassRoom.setProfessor(2);
//        saveClassRoom.setCollege(2);
//        saveClassRoom.setTerm(4001);
//        saveClassRoom.setCapacity(30);
//
//        classRoomService.createClassRoom(saveClassRoom);
//    }
//
//    public static void pickCourseByStudent()
//    {
//        ClassRoomService classRoomService = context.getBean(ClassRoomService.class);
//
//        PickOrDeleteClass pickClass = new PickOrDeleteClass();
//        pickClass.setClassId(1);
//        pickClass.setStudent(2);
//
//        try {
//            classRoomService.addStudent(pickClass);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void findClassRoom()
//    {
//        ClassRoomService classRoomService = context.getBean(ClassRoomService.class);
//
//        try {
//            ClassRoomDetail classRoomHome = classRoomService.findClassRoom(2);
//            System.out.println(classRoomHome);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }

//    public static void updateClassRoom()
//    {
//        ClassRoomService classRoomService = context.getBean(ClassRoomService.class);
//
//        ClassRoomDetail classRoomHome = new ClassRoomDetail();
//        classRoomHome.setClassId(2);
//        classRoomHome.setTerm(4001);
//
//        try {
//            classRoomService.updateClassRoom(classRoomHome);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void deleteCourseByStudent()
//    {
//        ClassRoomService classRoomService = context.getBean(ClassRoomService.class);
//
//        PickOrDeleteClass deleteClass = new PickOrDeleteClass();
//        deleteClass.setClassId(2);
//        deleteClass.setStudent(3);
//
//        try {
//            classRoomService.deleteStudent(deleteClass);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void deleteClassRoom()
//    {
//        ClassRoomService classRoomService = context.getBean(ClassRoomService.class);
//
//        try {
//            classRoomService.deleteClassRoom(1);
//        } catch (ExceptionMessage e) {
//            e.printStackTrace();
//        }
//    }
}
