package com.example.sm.controller;

import com.example.sm.dto.classRoom.ClassRoomDetail;
import com.example.sm.dto.classRoom.ClassRoomList;
import com.example.sm.dto.classRoom.CreateClassRoom;
import com.example.sm.service.ClassRoomService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/classrooms")
public class ClassRoomController {
    private final ClassRoomService classRoomService;



    public ClassRoomController(ClassRoomService classRoomService) {
        this.classRoomService = classRoomService;
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_INSTRUCTION_EMPLOYEE', 'ROLE_COLLEGE_MANAGER', 'ROLE_PROFESSOR')")
    public void createClassRoom(@Valid @RequestBody CreateClassRoom classRoom) {
        classRoomService.createClassRoom(classRoom);
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_INSTRUCTION_EMPLOYEE', 'ROLE_COLLEGE_MANAGER')")
    public List<ClassRoomList> getAllClassRooms() {
        return classRoomService.getAllClassRooms();
    }

    @GetMapping(path = "{id}")
    @PreAuthorize("hasAnyRole('ROLE_INSTRUCTION_EMPLOYEE', 'ROLE_COLLEGE_MANAGER', 'ROLE_PROFESSOR')")
    public ClassRoomDetail getClassRoomDetail(@PathVariable("id") int id) {
        return classRoomService.getClassRoom(id);
    }

    @DeleteMapping(path = "{id}")
    @PreAuthorize("hasAnyRole('ROLE_INSTRUCTION_EMPLOYEE', 'ROLE_COLLEGE_MANAGER', 'ROLE_PROFESSOR')")
    public void deleteClassRoom(@PathVariable("id") int id) {
        classRoomService.deleteClassRoom(id);
    }

    @PutMapping(path = "{id}")
    @PreAuthorize("hasAnyRole('ROLE_INSTRUCTION_EMPLOYEE', 'ROLE_COLLEGE_MANAGER', 'ROLE_PROFESSOR')")
    public void updateClassRoom(@PathVariable("id") int id, @Valid@RequestBody ClassRoomDetail classRoomHome) {
        classRoomService.updateClassRoom(id, classRoomHome);
    }

    @PutMapping(path = "pickCourse")
    @PreAuthorize("hasAnyRole('ROLE_STUDENT', 'ROLE_INSTRUCTION_EMPLOYEE')")
    public void pickClassByStudent(@RequestParam Integer classId, @RequestParam Integer studentId) {
        classRoomService.addStudent(classId, studentId);
    }

    @DeleteMapping(path = "deleteCourse")
    @PreAuthorize("hasAnyRole('ROLE_STUDENT', 'ROLE_INSTRUCTION_EMPLOYEE')")
    public void deleteCourseByStudent(@RequestParam Integer classId, @RequestParam Integer studentId) {
        classRoomService.deleteStudent(classId, studentId);
    }
}
