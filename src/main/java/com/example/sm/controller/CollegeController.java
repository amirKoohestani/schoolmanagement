package com.example.sm.controller;

import com.example.sm.dto.college.CollegeDetail;
import com.example.sm.dto.college.CollegeList;
import com.example.sm.dto.college.CreateCollege;
import com.example.sm.service.CollegeService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/colleges")
public class CollegeController
{
    private final CollegeService collegeService;



    public CollegeController(CollegeService collegeService) {
        this.collegeService = collegeService;
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_UNIVERSITY_MANAGER')")
    public void createCollege(@Valid @RequestBody CreateCollege saveCollege) {
        collegeService.createCollege(saveCollege);
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_UNIVERSITY_MANAGER')")
    public List<CollegeList> getColleges() {
        return collegeService.getAllColleges();
    }

    @GetMapping(path = "{id}")
    @PreAuthorize("hasAnyRole('ROLE_UNIVERSITY_MANAGER', 'ROLE_COLLEGE_MANAGER')")
    public CollegeDetail getCollegDetail(@PathVariable("id") int id) {
        return collegeService.getCollege(id);
    }

    @DeleteMapping(path = {"{id}"})
    @PreAuthorize("hasRole('ROLE_UNIVERSITY_MANAGER')")
    public void deleteCollege(@PathVariable("id") int id) {
        collegeService.deleteCollege(id);
    }

    @PutMapping(path = "{id}")
    @PreAuthorize("hasAnyRole('ROLE_UNIVERSITY_MANAGER', 'ROLE_COLLEGE_MANAGER')")
    public void updateCollege(@PathVariable("id") int id, @Valid @RequestBody CollegeDetail collegeDetail){
        collegeService.updateCollege(id, collegeDetail);
    }
}
