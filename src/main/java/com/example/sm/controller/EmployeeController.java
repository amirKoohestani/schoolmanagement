package com.example.sm.controller;

import com.example.sm.dto.ChangePassword;
import com.example.sm.dto.employee.CreateEmployee;
import com.example.sm.dto.employee.EmployeeDetail;
import com.example.sm.service.EmployeeService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController
{
    private final EmployeeService employeeService;



    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_COLLEGE_MANAGER')")
    public void createEmployee(@Valid @RequestBody CreateEmployee saveEmployee) {
        employeeService.createEmployee(saveEmployee);
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_COLLEGE_MANAGER')")
    public List<EmployeeDetail> getEmployees(){
        return employeeService.getAllEmployees();
    }

    @GetMapping(path = "{id}")
    @PreAuthorize("hasAnyRole('ROLE_COLLEGE_MANAGER', 'ROLE_INSTRUCTION_EMPLOYEE')")
    public EmployeeDetail getEmployeeDetail(@PathVariable("id") int id) {
        return employeeService.getEmployee(id);
    }

    @DeleteMapping(path = "{id}")
    @PreAuthorize("hasRole('ROLE_COLLEGE_MANAGER')")
    public void deleteEmployee(@PathVariable("id") int id) {
        employeeService.deleteEmployee(id);
    }

    @PutMapping(path = "{id}")
    @PreAuthorize("hasRole('ROLE_COLLEGE_MANAGER')")
    public void updateEmployee(@PathVariable("id") int id, @Valid @RequestBody EmployeeDetail employeeDetail) {
        employeeService.updateEmployee(id, employeeDetail);
    }

    @PutMapping(path = "changePassword")
    @PreAuthorize("hasRole('ROLE_INSTRUCTION_EMPLOYEE')")
    public void changePassword(@RequestBody ChangePassword changePassword) {
        employeeService.changePassword(changePassword);
    }
}
