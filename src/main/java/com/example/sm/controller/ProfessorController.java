package com.example.sm.controller;

import com.example.sm.dto.ChangePassword;
import com.example.sm.dto.professor.CreateProfessor;
import com.example.sm.dto.professor.ProfessorDetail;
import com.example.sm.dto.professor.ProfessorList;
import com.example.sm.dto.professor.UpdateProfessor;
import com.example.sm.service.ProfessorService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/professors")
public class ProfessorController {
    private final ProfessorService professorService;



    public ProfessorController(ProfessorService professorService) {
        this.professorService = professorService;
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_COLLEGE_MANAGER')")
    public void createProfessor(@Valid @RequestBody CreateProfessor saveProfessor) {
        professorService.createProfessor(saveProfessor);
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_INSTRUCTION_EMPLOYEE', 'ROLE_COLLEGE_MANAGER')")
    public List<ProfessorList> getProfessors() {
        return professorService.getAllProfessors();
    }

    @GetMapping(path = "{id}")
    @PreAuthorize("hasAnyRole('ROLE_PROFESSOR', 'ROLE_COLLEGE_MANAGER')")
    public ProfessorDetail getProfessorDetail(@PathVariable("id") int id) {
        return professorService.getProfessor(id);
    }

    @DeleteMapping(path = "{id}")
    @PreAuthorize("hasRole('ROLE_COLLEGE_MANAGER')")
    public void deleteProfessor(@PathVariable("id") int id) {
        professorService.deleteProfessor(id);
    }

    @PutMapping(path = "{id}")
    @PreAuthorize("hasRole('ROLE_PROFESSOR')")
    public void updateProfessor(@PathVariable("id") int id, @Valid @RequestBody UpdateProfessor updateProfessor) {
        professorService.updateProfessor(id, updateProfessor);
    }

    @PutMapping(path = "changePassword")
    @PreAuthorize("hasRole('ROLE_PROFESSOR')")
    public void changePassword(@RequestBody ChangePassword changePassword) {
        professorService.changePassword(changePassword);
    }
}
