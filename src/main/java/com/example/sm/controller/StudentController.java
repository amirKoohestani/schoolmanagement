package com.example.sm.controller;

import com.example.sm.dto.ChangePassword;
import com.example.sm.dto.Student.CreateStudent;
import com.example.sm.dto.Student.StudentDetail;
import com.example.sm.dto.Student.StudentList;
import com.example.sm.dto.Student.UpdateStudent;
import com.example.sm.service.StudentService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {
    private final StudentService studentService;



    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_INSTRUCTION_EMPLOYEE')")
    public void createStudent(@Valid @RequestBody CreateStudent createStudent) {
            studentService.createStudent(createStudent);
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_INSTRUCTION_EMPLOYEE', 'ROLE_COLLEGE_MANAGER')")
    public List<StudentList> getStudents() {
        return studentService.getAllStudents();
    }


    @GetMapping(path = "{id}")
    @PreAuthorize("hasAnyRole('ROLE_STUDENT', 'ROLE_INSTRUCTION_EMPLOYEE', 'ROLE_COLLEGE_MANAGER')")
    public StudentDetail getStudentDetail(@PathVariable("id") int id) {
        return studentService.getStudent(id);
    }

    @DeleteMapping(path = "{id}")
    @PreAuthorize("hasAnyRole('ROLE_INSTRUCTION_EMPLOYEE', 'ROLE_COLLEGE_MANAGER')")
    public void deleteStudent(@PathVariable("id") int id) {
        studentService.deleteStudent(id);
    }

    @PutMapping(path = "{id}")
    @PreAuthorize("hasRole('ROLE_INSTUCTION_EMPLOYEE')")
    public void updateStudent(@PathVariable("id") int id, @Valid @RequestBody UpdateStudent updateStudent) {
        studentService.updateStudent(id, updateStudent);
    }

    @PutMapping(path = "changePassword")
    @PreAuthorize("hasAnyRole('ROLE_INSTRUCTION_EMPLOYEE', 'ROLE_STUDENT')")
    public void changePassword(@RequestBody ChangePassword changePassword) {
        studentService.changePassword(changePassword);
    }
}
