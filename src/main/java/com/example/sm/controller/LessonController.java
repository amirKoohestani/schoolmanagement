package com.example.sm.controller;

import com.example.sm.dto.lesson.CreateLesson;
import com.example.sm.dto.lesson.LessonDetail;
import com.example.sm.dto.lesson.UpdateLesson;
import com.example.sm.service.LessonService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("lessons")
public class LessonController {
    private final LessonService lessonService;



    public LessonController(LessonService lessonService) {
        this.lessonService = lessonService;
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_COLLEGE_MANAGER')")
    public void createLesson(@Valid @RequestBody CreateLesson createLesson) {
        lessonService.createLesson(createLesson);
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_INSTRUCTION_EMPLOYEE', 'ROLE_COLLEGE_MANAGER')")
    public List<LessonDetail> getLessons() {
        return lessonService.getAllLessons();
    }

    @GetMapping(path = "{id}")
    @PreAuthorize("hasAnyRole('ROLE_INSTRUCTION_EMPLOYEE', 'ROLE_COLLEGE_MANAGER')")
    public LessonDetail getLessonDetail(@PathVariable("id") int id) {
        return lessonService.getLesson(id);
    }

    @DeleteMapping(path = "{id}")
    @PreAuthorize("hasRole('ROLE_COLLEGE_MANAGER')")
    public void deleteLesson(@PathVariable("id") int id) {
        lessonService.deleteLesson(id);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('ROLE_COLLEGE_MANAGER')")
    public void updateLesson(@PathVariable("id") int id, @Valid @RequestBody UpdateLesson updateLesson) {
        lessonService.updateLesson(id, updateLesson);
    }
}
