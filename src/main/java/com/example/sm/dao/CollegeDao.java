package com.example.sm.dao;

import com.example.sm.entity.College;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CollegeDao extends JpaRepository<College, Integer> {
    Boolean existsByName(String name);
}
