package com.example.sm.dao;

import com.example.sm.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface CourseDao extends JpaRepository< Course, CourseId> {
    Set<Course> findByIdStudentAndIdTerm(Student student, Integer term);
    Optional<Course> findByIdStudentAndIdLessonAndIdTerm(Student student, Lesson lesson, Integer term);
    Optional<Course> findByIdStudentAndClassRoom(Student student, ClassRoom classRoom);
}
