package com.example.sm.dao;

import com.example.sm.entity.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LessonDao extends JpaRepository< Lesson, Integer > {
    Boolean existsByNameAndUnit(String name, Integer unit);
}
