package com.example.sm.dao;

import com.example.sm.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentDao extends JpaRepository< Student, Integer > {
    Boolean existsByNationalCode(String nationalCode);
}
