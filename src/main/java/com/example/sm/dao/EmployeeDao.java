package com.example.sm.dao;

import com.example.sm.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeDao extends JpaRepository<Employee, Integer> {
    Boolean existsByNationalCode(String nationalCode);
}
