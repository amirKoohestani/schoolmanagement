package com.example.sm.dao;

import com.example.sm.entity.Professor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfessorDao extends JpaRepository< Professor, Integer > {
    Boolean existsByNationalCode(String nationalCode);
}
