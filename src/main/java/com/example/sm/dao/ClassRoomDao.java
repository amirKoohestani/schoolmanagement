package com.example.sm.dao;

import com.example.sm.entity.ClassRoom;
import com.example.sm.entity.Lesson;
import com.example.sm.entity.Professor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface ClassRoomDao extends JpaRepository< ClassRoom, Integer > {
    Set<ClassRoom> findByProfessorAndTerm(Professor professor, Integer term);
    Set<ClassRoom> findByProfessorAndLesson(Professor professor, Lesson lesson);
}
