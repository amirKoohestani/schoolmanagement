package com.example.sm.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
public class Course {
    private CourseId id;
    private ClassRoom classRoom;
    private float score;



    @EmbeddedId
    public CourseId getId() {
        return id;
    }

    public void setId(CourseId id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "classId", nullable = false)
    public ClassRoom getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(ClassRoom classRoom) {
        this.classRoom = classRoom;
    }

    @Min(value = 0)
    @Max(value = 20)
    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }
}
