package com.example.sm.entity;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.HashSet;
import java.util.Set;

public enum UserRole
{
    STUDENT,
    PROFESSOR,
    INSTRUCTION_EMPLOYEE,
    COLLEGE_MANAGER,
    UNIVERSITY_MANAGER;



    public Set<SimpleGrantedAuthority> getGrantedAuthorities()
    {
        Set<SimpleGrantedAuthority> permissions = new HashSet<>();

        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return permissions;
    }
}
