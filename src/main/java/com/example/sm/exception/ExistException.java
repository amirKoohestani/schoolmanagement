package com.example.sm.exception;

public class ExistException extends RuntimeException
{
    public ExistException(String message) {
        super(message);
    }
}
