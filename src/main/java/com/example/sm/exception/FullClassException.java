package com.example.sm.exception;

public class FullClassException extends RuntimeException{
    public FullClassException(String message) {
        super(message);
    }
}
