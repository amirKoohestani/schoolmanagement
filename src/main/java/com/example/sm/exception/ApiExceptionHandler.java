package com.example.sm.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<Object> handlerNotFoundException(
            NotFoundException exception, HttpServletRequest request) {
        ApiException apiException = new ApiException(
                exception.getMessage(),
                Instant.now().getEpochSecond(),
                request.getServletPath()
        );
        return new ResponseEntity<>(apiException, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ExistException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handlerExistException(
            ExistException exception, HttpServletRequest request)
    {
        ApiException apiException = new ApiException(
                exception.getMessage(),
                Instant.now().getEpochSecond(),
                request.getServletPath()
        );
        return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(WrongPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handlerExistException(
            WrongPasswordException exception, HttpServletRequest request)
    {
        ApiException apiException = new ApiException(
                exception.getMessage(),
                Instant.now().getEpochSecond(),
                request.getServletPath()
        );
        return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FullClassException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handlerExistException(
            FullClassException exception, HttpServletRequest request)
    {
        ApiException apiException = new ApiException(
                exception.getMessage(),
                Instant.now().getEpochSecond(),
                request.getServletPath()
        );
        return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
    }
}
