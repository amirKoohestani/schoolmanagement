package com.example.sm.exception;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiException {
    private final String message;
    private final Long timeStamp;
    private final String path;
    private Map<String, String> validationErrors;


    public ApiException(String message, Long timeStamp, String path) {
        this.message = message;
        this.timeStamp = timeStamp;
        this.path = path;
    }

    public String getMessage() {
        return message;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public String getPath() {
        return path;
    }

    public Map<String, String> getValidationErrors() {
        return validationErrors;
    }

    public void setValidationErrors(Map<String, String> validationErrors) {
        this.validationErrors = validationErrors;
    }
}
