package com.example.sm.dto.lesson;

import javax.validation.constraints.NotNull;

public class UpdateLesson {
    private String name;
    private Integer unit;



    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    public Integer getUnit() {
        return unit;
    }

    public void setUnit(Integer unit) {
        this.unit = unit;
    }
}
