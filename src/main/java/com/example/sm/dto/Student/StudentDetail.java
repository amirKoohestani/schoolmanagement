package com.example.sm.dto.Student;

import com.example.sm.entity.Address;

import java.util.HashSet;
import java.util.Set;

public class StudentDetail {
    private Integer studentId;
    private String firstName;
    private String lastName;
    private String nationalCode;
    private Address address;
    private String College;
    private Set<StudentsClass> studentsClasses = new HashSet<>();
    private Float average;



    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getCollege() {
        return College;
    }

    public void setCollege(String college) {
        College = college;
    }

    public Set<StudentsClass> getStudentsClasses() {
        return studentsClasses;
    }

    public void setStudentsClasses(Set<StudentsClass> studentsClasses) {
        this.studentsClasses = studentsClasses;
    }

    public Float getAverage() {
        return average;
    }

    public void setAverage(Float average) {
        this.average = average;
    }
}
