package com.example.sm.dto.college;

import java.util.Set;

public class CollegeDetail {
    private Integer collegeId;
    private String name;
    private Set<ProfessorOfCollege> professors;
    private Set<StudentOfCollege> students;
    private Float average;



    public Integer getCollegeId() {
        return collegeId;
    }

    public void setCollegeId(Integer collegeId) {
        this.collegeId = collegeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<ProfessorOfCollege> getProfessors() {
        return professors;
    }

    public void setProfessors(Set<ProfessorOfCollege> professors) {
        this.professors = professors;
    }

    public Set<StudentOfCollege> getStudents() {
        return students;
    }

    public void setStudents(Set<StudentOfCollege> students) {
        this.students = students;
    }

    public Float getAverage() {
        return average;
    }

    public void setAverage(Float average) {
        this.average = average;
    }
}
