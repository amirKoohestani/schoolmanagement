package com.example.sm.dto.college;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class CreateCollege implements Serializable {
    @NotBlank
    @Size(min = 2, max = 30)
    private String name;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
