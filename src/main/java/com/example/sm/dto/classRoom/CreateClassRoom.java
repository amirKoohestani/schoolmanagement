package com.example.sm.dto.classRoom;

import javax.validation.constraints.NotNull;

public class CreateClassRoom {
    private Integer professor;
    private Integer lesson;
    private Integer term;
    private Integer college;
    private Integer capacity;



    @NotNull
    public Integer getProfessor() {
        return professor;
    }

    public void setProfessor(Integer professor) {
        this.professor = professor;
    }

    @NotNull
    public Integer getLesson() {
        return lesson;
    }

    public void setLesson(Integer lesson) {
        this.lesson = lesson;
    }

    @NotNull
    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    @NotNull
    public Integer getCollege() {
        return college;
    }

    public void setCollege(Integer college) {
        this.college = college;
    }

    @NotNull
    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }
}