package com.example.sm.dto.classRoom;

import java.util.HashSet;
import java.util.Set;

public class ClassRoomDetail {
    private Integer classId;
    private Integer professor;
    private Integer lesson;
    private Integer term;
    private Integer college;
    private Integer capacity;
    private Integer studentNumbers;
    private Set<StudentOfClass> students = new HashSet<>();
    private Float average;



    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getProfessor() {
        return professor;
    }

    public void setProfessor(Integer professor) {
        this.professor = professor;
    }

    public Integer getLesson() {
        return lesson;
    }

    public void setLesson(Integer lesson) {
        this.lesson = lesson;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public Integer getCollege() {
        return college;
    }

    public void setCollege(Integer college) {
        this.college = college;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Integer getStudentNumbers() {
        return studentNumbers;
    }

    public void setStudentNumbers(Integer studentNumbers) {
        this.studentNumbers = studentNumbers;
    }

    public Set<StudentOfClass> getStudents() {
        return students;
    }

    public void setStudents(Set<StudentOfClass> students) {
        this.students = students;
    }

    public Float getAverage() {
        return average;
    }

    public void setAverage(Float average) {
        this.average = average;
    }
}
