package com.example.sm.dto.classRoom;

import javax.validation.constraints.NotNull;

public class PickOrDeleteClass {
    private Integer classId;
    private Integer student;



    @NotNull
    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    @NotNull
    public Integer getStudent() {
        return student;
    }

    public void setStudent(Integer student) {
        this.student = student;
    }
}
