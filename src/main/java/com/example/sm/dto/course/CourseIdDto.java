package com.example.sm.dto.course;

import javax.validation.constraints.NotNull;

public class CourseIdDto {
    private Integer student;
    private Integer lesson;
    private Integer term;



    @NotNull
    public Integer getStudent() {
        return student;
    }

    public void setStudent(Integer student) {
        this.student = student;
    }

    @NotNull
    public Integer getLesson() {
        return lesson;
    }

    public void setLesson(Integer lesson) {
        this.lesson = lesson;
    }

    @NotNull
    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }
}
