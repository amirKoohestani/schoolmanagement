package com.example.sm.dto.course;

import javax.validation.constraints.NotNull;

public class CreateCourse {
    private Integer Student;
    private Integer classRoom;



    @NotNull
    public Integer getStudent() {
        return Student;
    }

    public void setStudent(Integer student) {
        Student = student;
    }

    @NotNull
    public Integer getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(Integer classRoom) {
        this.classRoom = classRoom;
    }
}
