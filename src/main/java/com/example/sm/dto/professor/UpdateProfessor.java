package com.example.sm.dto.professor;

import com.example.sm.entity.Address;
import com.example.sm.entity.College;

import javax.validation.constraints.NotNull;

public class UpdateProfessor {
    private Address address;
    private College college;



    @NotNull
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @NotNull
    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }
}
