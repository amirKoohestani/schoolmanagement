package com.example.sm.dto.professor;

import com.example.sm.entity.Address;

import java.util.HashSet;
import java.util.Set;

public class ProfessorDetail {
    private Integer professorId;
    private String firstName;
    private String lastName;
    private String nationalCode;
    private Address address;
    private String college;
    private Set<ProfessorsClass> professorClasses = new HashSet<>();
    private Float studentsAverage;



    public Integer getProfessorId() {
        return professorId;
    }

    public void setProfessorId(Integer professorId) {
        this.professorId = professorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public Set<ProfessorsClass> getProfessorClasses() {
        return professorClasses;
    }

    public void setProfessorClasses(Set<ProfessorsClass> professorClasses) {
        this.professorClasses = professorClasses;
    }

    public Float getStudentsAverage() {
        return studentsAverage;
    }

    public void setStudentsAverage(Float studentsAverage) {
        this.studentsAverage = studentsAverage;
    }
}
